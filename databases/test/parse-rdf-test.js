'use strict';

const fs = require("fs");
const expect = require('chai').expect;

const rdf = fs.readFileSync(`${__dirname}/pg132.rdf`);
const parseRDF = require("../lib/parse-rdf.js");

describe('parseRDF', () =>{
    it('should be a function', () => {
        expect(parseRDF).to.be.a('function');
    });

    it("should parse RDF content", () =>{
        const book = parseRDF(rdf);
        expect(book).to.be.an("object");
        expect(book).to.have.property('id', 132);
    });

    it("should parse the RDF title", () =>{
        const book = parseRDF(rdf);
        expect(book).to.have.a.property("title", "The Art of War");
    });

    it("should check authors in an array", () => {
        const book = parseRDF(rdf);
        expect(book).to.have.a.property('authors')
        .that.is.an('array').with.lengthOf(2)
        .and.contains('Sunzi, active 6th century B.C.')
        .and.contains('Giles, Lionel');
    });

    it("should have these subjects", () => {
        const book = parseRDF(rdf);
        expect(book).to.have.property('subjects')
                    .that.is.an('array').with.lengthOf(2)
                    .and.contains('Military art and science -- Early works to 1800')
                    .and.contains('War -- Early works to 1800');
    });

    it("should have lcc", () => {
        const book = parseRDF(rdf);
        expect(book).to.have.property('lcc')
                    .that.is.an('string').with.length.above(0)
                    .and.to.match(/[^IOWXY]/);
    });

    it("should have array of download locations", () => {
        const book = parseRDF(rdf);
        expect(book).to.have.property("download")
                    .that.is.an("array").with.length.above(0);
    });
});