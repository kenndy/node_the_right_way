/***
 * Excerpted from "Node.js 8 the Right Way",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material,
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose.
 * Visit http://www.pragmaticprogrammer.com/titles/jwnode2 for more book information.
 ***/
'use strict';
const pkg = require('./package.json');
const {
  URL
} = require('url');
const path = require('path');

// nconf configuration.
const nconf = require('nconf');
nconf
  .argv()
  .env('__')
  .defaults({
    'NODE_ENV': 'development'
  });

const NODE_ENV = nconf.get('NODE_ENV');
const isDev = NODE_ENV === 'development';
nconf
  .defaults({
    'conf': path.join(__dirname, `${NODE_ENV}.config.json`)
  })
  .file(nconf.get('conf'));

const serviceUrl = new URL(nconf.get('serviceUrl'));
const servicePort =
  serviceUrl.port || (serviceUrl.protocol === 'https:' ? 443 : 80);

const fs = require("fs");
const https = require("https");
const httpsOptions = {
  key: fs.readFileSync(path.join(__dirname, "../tlskeys", nconf.get("tls:key"))),
  cert: fs.readFileSync(path.join(__dirname, "../tlskeys", nconf.get("tls:cert"))),
  requestCert: false,
  rejectUnauthorized: false
}

// Express and middleware.
const express = require('express');
const morgan = require('morgan');

const app = express();
const expressSession = require('express-session');
if (isDev) {
  // use FileStore in development mode
  const FileStore = require('session-file-store')(expressSession);
  app.use(expressSession({
    resave: false,
    saveUninitialized: true,
    secret: 'unguessable:D',
    store: new FileStore()
  }));
} else {
  // Use RedisStore in production mode.
  const RedisStore = require('connect-redis')(expressSession);
  app.use(expressSession({
    resave: false,
    saveUninitialized: false,
    secret: nconf.get('redis:secret'),
    store: new RedisStore({
      host: nconf.get('redis:host'),
      port: nconf.get('redis:port'),
    }),
  }));
}
const passport = require("passport");
passport.serializeUser((profile, done) => done(null, {
  id: profile.id,
  provider: profile.provider
}));
passport.deserializeUser((user, done) => done(null, user));
app.use(passport.initialize());
app.use(passport.session());
app.use(morgan('dev'));

app.get('/api/version', (req, res) => res.status(200).json(pkg.version));

// Serve webpack assets.
if (isDev) {
  const webpack = require('webpack');
  const webpackMiddleware = require('webpack-dev-middleware');
  const webpackConfig = require('./webpack.config.js');
  app.use(webpackMiddleware(webpack(webpackConfig), {
    publicPath: '/',
    stats: {
      colors: true
    },
  }));
} else {
  app.use(express.static('dist'));
}

app.get('/api/session', (req, res) => {
  const session = {
    auth: req.isAuthenticated()
  };
  res.status(200).json(session);
});

app.get('/auth/signout', (req, res) => {
  req.logout();
  res.redirect('/');
});

const FacebookStrategy = require('passport-facebook').Strategy;
passport.use(new FacebookStrategy({
  clientID: nconf.get('auth:facebook:appID'),
  clientSecret: nconf.get('auth:facebook:appSecret'),
  callbackURL: new URL('/auth/facebook/callback', serviceUrl).href,
}, (accessToken, refreshToken, profile, done) => done(null, profile)));
app.get('/auth/facebook', passport.authenticate('facebook'));
app.get('/auth/facebook/callback', passport.authenticate('facebook', {
  successRedirect: '/#_=_',
  failureRedirect: '/#_=_',
}));

const twitterStrategy = require('passport-twitter').Strategy;
passport.use(new twitterStrategy({
  consumerKey: nconf.get('auth:twitter:consumerKey'),
  consumerSecret: nconf.get('auth:twitter:consumerSecret'),
  callbackURL: new URL('/auth/twitter/callback', serviceUrl).href,
}, (accessToken, refreshToken, profile, done) => done(null, profile)));
app.get('/auth/twitter', passport.authenticate('twitter'));
app.get('/auth/twitter/callback', passport.authenticate('twitter', {
  successRedirect: '/',
  failureRedirect: '/',
}));

const googleStrategy = require('passport-google-oauth20').Strategy;
passport.use(new googleStrategy({
  clientID: nconf.get('auth:google:client_id'),
  clientSecret: nconf.get('auth:google:client_secret'),
  callbackURL: new URL('/auth/google/callback', serviceUrl).href,
  scope: 'https://www.googleapis.com/auth/plus.login',

}, (accessToken, refreshToken, profile, done) => done(null, profile)));
app.get('/auth/google', passport.authenticate('google', {
  scope: ['email', 'profile']
}));
app.get('/auth/google/callback', passport.authenticate('google', {
  successRedirect: '/#_=_',
  failureRedirect: '/#_=_',
}));

app.use('/api', require('./lib/bundle.js')(nconf.get('es')));


https.createServer(httpsOptions, app)
  .listen(servicePort, () => console.log('Ready.'));