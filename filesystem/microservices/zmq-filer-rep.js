"use strict";
const fs = require('fs');
const zmq = require('zeromq');

// socket to reply to client request
const responder = zmq.socket("rep");

//handle incoming request
responder.on("message", data=> {
    // parse the incoming message
    const request = JSON.parse(data);
    console.log(`Received request to get: ${request.path}`);

    // read the file an repl with content
    fs.readFile(request.path, (err, content) => {
        console.log("sending response content.");
        responder.send(JSON.stringify({
            content: content.toString(),
            timestamp: Date.now(),
            pid: process.pid
        }));
    });
});

// listen to TCP port 60401
responder.bind("tcp://127.0.0.1:60401", err => {
    console.log("listening for zmq requesters...");
});

// close the responsder when the node proess ends.
process.on('SIGINT', () => {
    console.log('shutting down ...');
    responder.close();
});