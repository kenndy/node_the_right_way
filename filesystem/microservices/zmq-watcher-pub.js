'use strict';
import { watch } from "fs";
import { socket } from "zeromq";
const filename = process.argv[2];

// create the publisher endpoint
const publisher = socket("pub");

watch(filename, () => {
    // send message to any and all subscribers
    publisher.send(JSON.stringify({
        type: 'changed',
        file: filename,
        timestamp: Date.now()
    }));
});

//listen to tcp port 60400
publisher.bind('tcp://*:60400', err => {
    if(err) {
        throw err;
    }
    console.log('Lishening for zmq subscribers...');
});