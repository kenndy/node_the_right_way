import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";
import * as templates from './templates.ts';


document.body.innerHTML = templates.main();
const mainElement = document.body.querySelector('.b4-main');
const alertsElement = document.body.querySelector('.b4-alerts');

const getBundles = async () => {
    const esRes = await fetch('/es/b4/bundle/_search?size=1000');
    const esResBody = await esRes.json();
    return esResBody.hits.hits.map(hit => ({
        id: hit._id,
        name: hit._source.name
    }));
};

/**
 * Show an alert to the user
 */
const showAlert = (message, type = "danger") => {
    const html = templates.alert({ type, message });
    alertsElement.insertAdjacentHTML('beforeend', html);
};

/**
 * Create a new bundle with the given name, then list bundles
 */
const addBundle = async (name) => {
    try {
        const bundle = await getBundles();
        const url = `/api/bundle?name=${encodeURIComponent(name)}`;
        const res = await fetch(url, { method: 'POST' });
        const resBody = await res.json();

        bundle.push({ id: resBody._id, name });
        listBundles(bundle);
        showAlert(`Bundle "${name}" created!`, 'success');
    } catch (err) {
        showAlert(err);
    }
};

const listBundles = bundles => {
    mainElement.innerHTML = templates.addBundleForm() +
        templates.listBundles({ bundles });
    const form = mainElement.querySelector('form');
    form.addEventListener('submit', event => {
        event.preventDefault();
        const name = form.querySelector('input').value;
        addBundle(name);
    });
    const deleteButtons = mainElement.querySelectorAll("button.delete");
    deleteButtons.forEach(deleteButton => {
        deleteButton.addEventListener('click', event => {
            deleteBundle(deleteButton.getAttribute('data-bundle-id'));
        });
    });
};

/**
 * Delete the bundle with the specified ID, then list bundles.
 */
const deleteBundle = async (bundleId) => {
    try {
        const bundles = await getBundles();
        const url = `/api/bundle/${encodeURIComponent(bundleId)}`;
        const idx = bundles.findIndex(bundle => bundle.id === bundleId);
        await fetch(url, { method: "DELETE" });
        bundles.splice(idx, 1);
        listBundles(bundles);
        showAlert(`Bundle deleted!`, 'success');
    } catch (err) {
        showAlert(err);
    }
}

/**
 * Use Window location hash to show the specific view
 */
const showView = async () => {
    const [view, ...params] = window.location.hash.split('/');

    switch (view) {
        case '#welcome':
            mainElement.innerHTML = templates.welcome();
            break;
        case '#list-bundles':
            const bundles = await getBundles();
            listBundles(bundles);
            break;
        default:
            // Unrecognized view.
            window.location.hash = '#welcome';
    }
};
window.addEventListener('hashchange', showView, false);
showView().catch(err => window.location.hash = '#welcome');




