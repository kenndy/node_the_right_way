'use strict';
const netClient = require('net').connect('/tmp/watcher.sock');
const ldjClient = require('./lib/ldj-client.js').connect(netClient);

ldjClient.on('message', message=>{
    if(message.type === 'watching'){
        console.log(`Now Watching: ${message.file}`);
    } else if (message.type === 'changed'){
        console.log(`File Changed: ${new Date(message.timestamp)}`);
    } else {
        throw Error(`Unrecognized message type: ${message.type}`);
    }
});