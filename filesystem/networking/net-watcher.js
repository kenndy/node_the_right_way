'use strict';
const fs = require("fs");
const net = require('net');
const filename = process.argv[2];

if(!filename){
    throw Error("Error: no filename specified");
}

net.createServer(connection =>{
    // Reporting
    console.log('Subscriber connected. ');
    connection.write(JSON.stringify({
        type: 'watching',
        file: filename
    }) +'\n');

    // Watcher Setup
    const watcher = fs.watch(filename, () =>
        connection.write(JSON.stringify({
            type: 'changed',
            timestamp: Date.now()
        }) + '\n')
    );

    // Cleanup
    connection.on('close', ()=>{
        console.log('subscriber disconnected');
        watcher.close();
    });
}).listen('/tmp/watcher.sock', () => console.log('lishening for subscribers...'));