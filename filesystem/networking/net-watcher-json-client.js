'use strict';
const net = require('net');
const client = net.connect('/tmp/watcher.sock');
client.on('data', data =>{
    const message = JSON.parse(data);
    if(message.type === 'watching'){
        console.log(`Now watching: ${message.file}`);
    }else if (message.type === 'changed') {
        const date = new Date(message.timestamp);
        console.log(`File changed: ${date}`);
    }else{
        throw Error(`Invalid message type:  ${message.type}`)
    }
} )