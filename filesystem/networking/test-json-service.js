'use strict';
const server = require('net').createServer(
    connection => {
        console.log('Subscriber connected.');

        //two message chunks that totget amke a whole message
        const firstChunk = '{"type":"changed", "timesta';
        const secondChunk = 'mp": 1519211810362}\n'

        // send the first chunk immediately
        connection.write(firstChunk);

        const timer = setTimeout(() => {
            connection.write(secondChunk);
            connection.end();
        }, 100);

        // clear timer when the connection ends
        connection.on('end', ()=>{
            clearTimeout(timer);
            console.log('subscriber disconnected')
        })
    }
)

server.listen('/tmp/watcher.sock', function(){
    console.log('Test server listening for subscribers...')
});