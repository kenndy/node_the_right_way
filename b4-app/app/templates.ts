import * as HandleBars from "../node_modules/handlebars/dist/handlebars.js";

export const main = HandleBars.compile(`
<div class="container">
<h1> B4 - Book Bundler</h1>
<div class="b4-alerts"></div>
<div class="b4-main"></div>
</div>
`);

export const welcome = HandleBars.compile(`
<div class="jumbotron">
<h1>Welcome!</h1>
<p>B4 is an application for creating book bundles.</p>
</div>
`);

export const alert = HandleBars.compile(`
<div class="alert alert-{{type}} alert-dismissible fade show active   " role="alert">
    <button class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    {{message}}
  </div>
`);

export const listBundles = HandleBars.compile(`
  <div class="card">
    <div class="card-header">Your Bundles</div>
    {{#if bundles.length}}
      <table class="table table-hover">
        <tr class="thead-default">
          <th scope="col">Bundle Name</th>
          <th scope="col">Actions</th>
        </tr>
        {{#each bundles}}
        <tr>
          <td>
            <a href="#view-bundle/{{id}}">{{name}}</a>
          </td>
          <td>
            <button class="btn delete" data-bundle-id="{{id}}">Delete</button>
          </td>
        </tr>
        {{/each}}
      </table>
    {{else}}
      <div class="panel-body">
        <p>None yet!</p>
      </div>
    {{/if}}
  </div>
`);

export const addBundleForm = HandleBars.compile(`
  <div class="card">
    <div class="card-header">Create a new bundle.</div>
    <div class="card-body">
      <form>
        <div class="input-group">
          <input class="form-control" placeholder="Bundle Name" />
          <span class="input-group-append">
            <button class="btn btn-primary" type="submit">Create</button>
          </span>
        </div>
      </form>
    </div>
  </div>
`);